﻿Public Class juego
    Private TurnoActual As Integer
    Private CantidadJugadores As Integer
    Private JugadasJugador1 As New List(Of Integer)
    Private JugadasJugador2 As New List(Of Integer)
    Private JugadasJugador3 As New List(Of Integer)
    Private JugadasJugador4 As New List(Of Integer)
    Private JugadasJugador5 As New List(Of Integer)
    Private JugadasJugador6 As New List(Of Integer)

    Public Sub New(ByVal cant As Integer)
        InitializeComponent()
        CantidadJugadores = cant
    End Sub
    Private Sub Juego_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TurnoActual = 0
        Cambiarturno()
        TextBox1.Text = 0
        TextBox2.Text = 0
        TextBox3.Text = 0
        TextBox4.Text = 0
        TextBox5.Text = 0
        TextBox6.Text = 0
    End Sub
    Private Sub Cambiarturno()
        If TurnoActual + 1 > CantidadJugadores Then
            TurnoActual = 1
        Else
            TurnoActual += 1
        End If
        turno.Text = TurnoActual
        HabilitarCheck(False)
        finalizarTurno.Enabled = False
        buttonTirarDados.Enabled = True
        cantidadTiros.Text = 0
        textbox7.Text = 0
        textbox8.Text = 0
        textbox9.Text = 0
        textbox10.Text = 0
        textbox11.Text = 0
        textbox12.Text = 0
        textbox13.Text = 0
        textbox14.Text = 0
        textbox15.Text = 0
        TextBox16.Text = 0
        TextBox17.Text = 0
        HabilitarPuntosMarcajes()
    End Sub
    Private Sub buttonTirarDados_Click(sender As Object, e As EventArgs) Handles buttonTirarDados.Click
        Dim tiros As Integer
        tiros = Integer.Parse(cantidadTiros.Text)
        tiros += 1
        Cantidadtiros.Text = tiros
        TirarDados()

        If tiros = 3 Then
            buttonTirarDados.Enabled = False
        ElseIf tiros = 1 Then
            HabilitarCheck(True)
        End If

        CalcularPuntosMarcajes()
    End Sub
    Private Sub CalcularPuntosMarcajes()
        TextBox7.Text = CalcularPuntos(1)
        TextBox8.Text = CalcularPuntos(2)
        TextBox9.Text = CalcularPuntos(3)
        TextBox10.Text = CalcularPuntos(4)
        TextBox11.Text = CalcularPuntos(5)
        TextBox12.Text = CalcularPuntos(6)
        TextBox13.Text = CalcularPuntos(7)
        TextBox14.Text = CalcularPuntos(8)
        TextBox15.Text = CalcularPuntos(9)
        TextBox16.Text = CalcularPuntos(10)
        TextBox17.Text = CalcularPuntos(11)
    End Sub
    Private Sub TirarDados()
        Dim Rnd As New Random()
        If Not CheckDado1.Checked Then
            CargarImagen(1, Rnd.Next(1, 7))
        End If
        If Not CheckDado2.Checked Then
            CargarImagen(2, Rnd.Next(1, 7))
        End If
        If Not CheckDado3.Checked Then
            CargarImagen(3, Rnd.Next(1, 7))
        End If
        If Not CheckDado4.Checked Then
            CargarImagen(4, Rnd.Next(1, 7))
        End If
        If Not CheckDado5.Checked Then
            CargarImagen(5, Rnd.Next(1, 7))
        End If
    End Sub

    Private Sub CargarImagen(dadoNumero As Integer, i As Integer)
        Dim pictureDado = CType(Me.Controls("PictureBox" + dadoNumero.ToString()), PictureBox)
        Select Case i
            Case 1
                pictureDado.Image = My.Resources._1
                pictureDado.Text = "1"
            Case 2
                pictureDado.Image = My.Resources._2
                pictureDado.Text = "2"
            Case 3
                pictureDado.Image = My.Resources._3
                pictureDado.Text = "3"
            Case 4
                pictureDado.Image = My.Resources._4
                pictureDado.Text = "4"
            Case 5
                pictureDado.Image = My.Resources._5
                pictureDado.Text = "5"
            Case 6
                pictureDado.Image = My.Resources._6
                pictureDado.Text = "6"
        End Select
    End Sub
    Private Sub HabilitarPuntosMarcajes()
        RadioButton1.Enabled = True
        RadioButton2.Enabled = True
        RadioButton3.Enabled = True
        RadioButton4.Enabled = True
        RadioButton5.Enabled = True
        RadioButton6.Enabled = True
        RadioButton7.Enabled = True
        RadioButton8.Enabled = True
        RadioButton9.Enabled = True
        RadioButton10.Enabled = True
        RadioButton11.Enabled = True

        If CantidadJugadores = 2 Then
            If JugadasJugador2.Count < 11 Then
                CheckPoint()
            Else
                ShareWinner()
            End If
        End If

        If CantidadJugadores = 3 Then
            If JugadasJugador3.Count < 11 Then
                CheckPoint()
            Else
                ShareWinner()
            End If
        End If

        If CantidadJugadores = 4 Then
            If JugadasJugador4.Count < 11 Then
                CheckPoint()
            Else
                ShareWinner()
            End If
        End If

        If CantidadJugadores = 5 Then
            If JugadasJugador5.Count < 11 Then
                CheckPoint()
            Else
                ShareWinner()
            End If
        End If

        If CantidadJugadores = 6 Then
            If JugadasJugador6.Count < 11 Then
                CheckPoint()
            Else
                ShareWinner()
            End If
        End If

    End Sub

    Private Function CheckPoint()
        If TurnoActual = 1 Then
            If JugadasJugador1.Count > 0 Then
                For i As Integer = 0 To JugadasJugador1.Count - 1
                    DeshabilitarRadio(JugadasJugador1(i))
                Next
            End If
        End If
        If TurnoActual = 2 Then
            If JugadasJugador2.Count > 0 Then
                For i As Integer = 0 To JugadasJugador2.Count - 1
                    DeshabilitarRadio(JugadasJugador2(i))
                Next
            End If
        End If
        If TurnoActual = 3 Then
            If JugadasJugador3.Count > 0 Then
                For i As Integer = 0 To JugadasJugador3.Count - 1
                    DeshabilitarRadio(JugadasJugador3(i))
                Next
            End If
        End If
        If TurnoActual = 4 Then
            If JugadasJugador4.Count > 0 Then
                For i As Integer = 0 To JugadasJugador4.Count - 1
                    DeshabilitarRadio(JugadasJugador4(i))
                Next
            End If
        End If
        If TurnoActual = 5 Then
            If JugadasJugador5.Count > 0 Then
                For i As Integer = 0 To JugadasJugador5.Count - 1
                    DeshabilitarRadio(JugadasJugador5(i))
                Next
            End If
        End If
        If TurnoActual = 6 Then
            If JugadasJugador6.Count > 0 Then
                For i As Integer = 0 To JugadasJugador6.Count - 1
                    DeshabilitarRadio(JugadasJugador6(i))
                Next
            End If
        End If
    End Function

    Private Function ShareWinner()
        Dim ganador = "Jugador 1"
        Dim pun As New List(Of Integer)
        pun.Add(Integer.Parse(TextBox1.Text))
        pun.Add(Integer.Parse(TextBox2.Text))
        pun.Add(Integer.Parse(TextBox3.Text))
        pun.Add(Integer.Parse(TextBox4.Text))
        pun.Add(Integer.Parse(TextBox5.Text))
        pun.Add(Integer.Parse(TextBox6.Text))
        Dim punGanador = pun(0)
        Dim sumaEmpate As Integer = 0

        For i As Integer = 1 To pun.Count - 1
            If pun(i) > punGanador Then
                If i = 1 Then
                    ganador = Label1.Text
                End If
                If i = 2 Then
                    ganador = Label2.Text
                End If
                If i = 3 Then
                    ganador = Label4.Text
                End If
                If i = 4 Then
                    ganador = Label4.Text
                End If
                If i = 5 Then
                    ganador = Label5.Text
                End If
                If i = 6 Then
                    ganador = Label6.Text
                End If
                punGanador = pun(i)
            End If
        Next
        For i As Integer = 0 To pun.Count - 1
            If pun(i) = punGanador Then
                sumaEmpate = sumaEmpate + 1
            End If
        Next

        If sumaEmpate = 1 Then
            MessageBox.Show("El ganador es el " + ganador)
            Me.Close()
        Else
            MessageBox.Show("Hay un empate en " + punGanador.ToString() + " puntos")
            Me.Close()
        End If
    End Function
    Private Sub DeshabilitarRadio(movimientoJugado As Integer)
        Select Case movimientoJugado
            Case 1
                RadioButton1.Enabled = False
            Case 2
                RadioButton2.Enabled = False
            Case 3
                RadioButton3.Enabled = False
            Case 4
                RadioButton4.Enabled = False
            Case 5
                RadioButton5.Enabled = False
            Case 6
                RadioButton6.Enabled = False
            Case 7
                RadioButton7.Enabled = False
            Case 8
                RadioButton8.Enabled = False
            Case 9
                RadioButton9.Enabled = False
            Case 10
                RadioButton10.Enabled = False
            Case 11
                RadioButton11.Enabled = False
        End Select
    End Sub

    Private Sub HabilitarCheck(b As Boolean)
        CheckDado1.Checked = False
        CheckDado2.Checked = False
        CheckDado3.Checked = False
        CheckDado4.Checked = False
        CheckDado5.Checked = False

        CheckDado1.Enabled = b
        CheckDado2.Enabled = b
        CheckDado3.Enabled = b
        CheckDado4.Enabled = b
        CheckDado5.Enabled = b

        RadioButton1.Checked = False
        RadioButton2.Checked = False
        RadioButton3.Checked = False
        RadioButton4.Checked = False
        RadioButton5.Checked = False
        RadioButton6.Checked = False
        RadioButton7.Checked = False
        RadioButton8.Checked = False
        RadioButton9.Checked = False
        RadioButton10.Checked = False
        RadioButton11.Checked = False
    End Sub
    Private Sub finalizarTurno_Click(sender As Object, e As EventArgs) Handles finalizarturno.Click
        If TurnoActual = 1 Then
            TextBox1.Text = Integer.Parse(TextBox1.Text) + AsignarPuntaje()
        End If
        If TurnoActual = 2 Then
            TextBox2.Text = Integer.Parse(TextBox2.Text) + AsignarPuntaje()
        End If
        If TurnoActual = 3 Then
            TextBox3.Text = Integer.Parse(TextBox3.Text) + AsignarPuntaje()
        End If
        If TurnoActual = 4 Then
            TextBox4.Text = Integer.Parse(TextBox4.Text) + AsignarPuntaje()
        End If
        If TurnoActual = 4 Then
            TextBox4.Text = Integer.Parse(TextBox5.Text) + AsignarPuntaje()
        End If
        If TurnoActual = 6 Then
            TextBox4.Text = Integer.Parse(TextBox6.Text) + AsignarPuntaje()
        End If
        Cambiarturno()
        PictureBox1.Image = My.Resources.empty
        PictureBox2.Image = My.Resources.empty
        PictureBox3.Image = My.Resources.empty
        PictureBox4.Image = My.Resources.empty
        PictureBox5.Image = My.Resources.empty

    End Sub
    Private Function AsignarPuntaje() As Integer
        Dim puntajeSeleccionado As Integer
        If RadioButton1.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox7.Text)
            AsignarMovimientoJugador(1)
        End If
        If RadioButton2.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox8.Text)
            AsignarMovimientoJugador(2)
        End If
        If RadioButton3.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox9.Text)
            AsignarMovimientoJugador(3)
        End If
        If RadioButton4.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox10.Text)
            AsignarMovimientoJugador(4)
        End If
        If RadioButton5.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox11.Text)
            AsignarMovimientoJugador(5)
        End If
        If RadioButton6.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox12.Text)
            AsignarMovimientoJugador(6)
        End If
        If RadioButton7.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox13.Text)
            AsignarMovimientoJugador(7)
        End If
        If RadioButton8.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox14.Text)
            AsignarMovimientoJugador(8)
        End If
        If RadioButton9.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox15.Text)
            AsignarMovimientoJugador(9)
        End If
        If RadioButton10.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox16.Text)
            AsignarMovimientoJugador(10)
        End If
        If RadioButton11.Checked Then
            puntajeSeleccionado = Integer.Parse(TextBox17.Text)
            AsignarMovimientoJugador(11)
        End If
        Return puntajeSeleccionado
    End Function
    Private Sub AsignarMovimientoJugador(i As Integer)
        If TurnoActual = 1 Then
            JugadasJugador1.Add(i)
        End If
        If TurnoActual = 2 Then
            JugadasJugador2.Add(i)
        End If
        If TurnoActual = 3 Then
            JugadasJugador3.Add(i)
        End If
        If TurnoActual = 4 Then
            JugadasJugador4.Add(i)
        End If
        If TurnoActual = 5 Then
            JugadasJugador5.Add(i)
        End If
        If TurnoActual = 6 Then
            JugadasJugador6.Add(i)
        End If
    End Sub
    Private Function CalcularPuntos(marcaje As Integer) As String
        Dim puntos As Integer
        Dim repetido1 As Integer
        Dim repetido2 As Integer
        Dim repetido3 As Integer
        Dim repetido4 As Integer
        Dim repetido5 As Integer
        Dim repetido6 As Integer
        Select Case marcaje
            Case 1
                repetido1 = ContarDados(1)
                puntos = repetido1
            Case 2
                repetido2 = ContarDados(2)
                puntos = repetido2 * 2
            Case 3
                repetido3 = ContarDados(3)
                puntos = repetido3 * 3
            Case 4
                repetido4 = ContarDados(4)
                puntos = repetido4 * 4
            Case 5
                repetido5 = ContarDados(5)
                puntos = repetido5 * 5
            Case 6
                repetido6 = ContarDados(6)
                puntos = repetido6 * 6
            Case 7 'Escalera
                repetido1 = ContarDados(1)
                repetido2 = ContarDados(2)
                repetido3 = ContarDados(3)
                repetido4 = ContarDados(4)
                repetido5 = ContarDados(5)
                repetido6 = ContarDados(6)
                If (((repetido1 = 1 And repetido2 = 1 And repetido3 = 1 And repetido4 = 1 And repetido5 = 1) Or (repetido2 = 1 And repetido3 = 1 And repetido4 = 1 And repetido5 = 1 And repetido6 = 1)) And VerificarChekeds()) Then
                    puntos = 20
                End If
                If (((repetido1 = 1 And repetido2 = 1 And repetido3 = 1 And repetido4 = 1 And repetido5 = 1) Or (repetido2 = 1 And repetido3 = 1 And repetido4 = 1 And repetido5 = 1 And repetido6 = 1)) And Not VerificarChekeds()) Then
                    puntos = 25
                End If
            Case 8 'Full
                repetido1 = ContarDados(1)
                repetido2 = ContarDados(2)
                repetido3 = ContarDados(3)
                repetido4 = ContarDados(4)
                repetido5 = ContarDados(5)
                repetido6 = ContarDados(6)
                If (((repetido1 = 3 Or repetido2 = 3 Or repetido3 = 3 Or repetido4 = 3 Or repetido5 = 3 Or repetido6 = 3) And (repetido1 = 2 Or repetido2 = 2 Or repetido3 = 2 Or repetido4 = 2 Or repetido5 = 2 Or repetido6 = 2)) And VerificarChekeds()) Then
                    puntos = 30
                End If
                If (((repetido1 = 3 Or repetido2 = 3 Or repetido3 = 3 Or repetido4 = 3 Or repetido5 = 3 Or repetido6 = 3) And (repetido1 = 2 Or repetido2 = 2 Or repetido3 = 2 Or repetido4 = 2 Or repetido5 = 2 Or repetido6 = 2)) And Not VerificarChekeds()) Then
                    puntos = 35
                End If
            Case 9 'Poker
                repetido1 = ContarDados(1)
                repetido2 = ContarDados(2)
                repetido3 = ContarDados(3)
                repetido4 = ContarDados(4)
                repetido5 = ContarDados(5)
                repetido6 = ContarDados(6)
                If ((repetido1 = 4 Or repetido2 = 4 Or repetido3 = 4 Or repetido4 = 4 Or repetido5 = 4 Or repetido6 = 4) And VerificarChekeds()) Then
                    puntos = 40
                End If
                If ((repetido1 = 4 Or repetido2 = 4 Or repetido3 = 4 Or repetido4 = 4 Or repetido5 = 4 Or repetido6 = 4) And Not VerificarChekeds()) Then
                    puntos = 45
                End If
            Case 10 'Generala
                repetido1 = ContarDados(1)
                repetido2 = ContarDados(2)
                repetido3 = ContarDados(3)
                repetido4 = ContarDados(4)
                repetido5 = ContarDados(5)
                repetido6 = ContarDados(6)
                If (repetido1 = 5 Or repetido2 = 5 Or repetido3 = 5 Or repetido4 = 5 Or repetido5 = 5 Or repetido6 = 5) Then
                    puntos = 50
                End If
            Case 11 'generala doble
                repetido1 = ContarDados(1)
                repetido2 = ContarDados(2)
                repetido3 = ContarDados(3)
                repetido4 = ContarDados(4)
                repetido5 = ContarDados(5)
                repetido6 = ContarDados(6)
                If (repetido1 = 5 Or repetido2 = 5 Or repetido3 = 5 Or repetido4 = 5 Or repetido5 = 5 Or repetido6 = 5) And RadioButton10.Enabled = False Then
                    puntos = 60
                End If
        End Select
        Return puntos
    End Function
    Private Function VerificarChekeds() As Boolean
        If CheckDado1.Checked Then
            Return True
        End If
        If CheckDado2.Checked Then
            Return True
        End If
        If CheckDado3.Checked Then
            Return True
        End If
        If CheckDado4.Checked Then
            Return True
        End If
        If CheckDado5.Checked Then
            Return True
        End If
    End Function
    Private Function ContarDados(valor As Integer) As Integer
        Dim puntos As Integer = 0
        Dim numero As String = valor.ToString()
        If PictureBox1.Text = numero Then
            puntos += 1
        End If
        If PictureBox2.Text = numero Then
            puntos += 1
        End If
        If PictureBox3.Text = numero Then
            puntos += 1
        End If
        If PictureBox4.Text = numero Then
            puntos += 1
        End If
        If PictureBox5.Text = numero Then
            puntos += 1
        End If
        Return puntos
    End Function
    Private Sub radio1_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radio2_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radio3_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radio4_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radio5_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radio6_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radioTrio_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radioFull_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radioPoker_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub

    Private Sub radioEscalera_CheckedChanged(sender As Object, e As EventArgs)
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio4_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton4.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio5_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton5.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio6_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton6.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio7_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton7.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio8_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton8.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio9_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton9.CheckedChanged
        finalizarturno.Enabled = True
    End Sub
    Private Sub checkedRadio10_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton10.CheckedChanged
        finalizarturno.Enabled = True
    End Sub

    Private Sub checkedRadio11_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton11.CheckedChanged
        finalizarturno.Enabled = True
    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub Label6_Click(sender As Object, e As EventArgs) Handles Label6.Click

    End Sub

    Private Sub CantidadTiros_Click(sender As Object, e As EventArgs) Handles Cantidadtiros.Click

    End Sub

    Private Function Rnd() As Integer
        Throw New NotImplementedException
    End Function


End Class